library("DT");
library("rjson");
library("shiny");
# library("rHighcharts");
# library("rCharts");
library("shinythemes");
library("highcharter");
library("dplyr");
library("reshape2")
library("xlsx");
library("httr");
library("devtools");
library("ggplot2");
library("quantmod");
library("scales")
#library("sqldf");
library("RPostgreSQL");
library("DBI")
library("shinyBS");
library("ineq");
library("anytime")
library("rpivotTable");
library("gsubfn")

################ VARIABLES ##################
{
  var <- c("EMPRESAS","PROCEDIMIENTOS","MONTO ADJUDICADO (en millones)")
  var_procesos <- c("PROCESOS","MONTO")
  var_procesos_tipo <- c("EXTREMO", "ALTO", "MEDIO", "BAJO", "INSIGNIFICANTE")
  riesgo <- c("TODOS", "EXTREMO", "ALTO", "MEDIO", "BAJO","INSIGNIFICANTE")
  GRUPO <- c("ENTIDADES","PROVEEDORES PUBLICOS","PROVEEDORES PRIVADOS")
  GRUPO_PROCESOS <- c("RESUMEN","RANKING", "ENTIDADES", "TABLA DINÁMICA")
  consolidado <- c("Publicados","Ofertas")
  ZONAS <- c("GLOBAL",
             "Zona 1: Esmeraldas, Imbabura, Carchi, Sucumbios.",
             "Zona 2: Pichincha (excepto Quito), Napo, Orellana.",
             "Zona 3: Cotopaxi, Tungurahua, Chimborazo, Pastaza.",
             "Zona 4: Manabi, Santo Domingo de los Tsachilas.",
             "Zona 5: Santa Elena, Guayas (sin Zona 8), Bolivar, Los Rios y Galapagos.",
             'Zona 6: Canar, Azuay, Morona Santiago.',
             "Zona 7: El Oro, Loja, Zamora Chinchipe.",
             "Zona 8: Guayaquil, Samborondon y Duran.",
             "Zona 9: Distrito Metropolitano de Quito.")
  
  SELEC_VAR <- "Seleccione la variable a visualizar"
  SELEC_RIES <- "Seleccione el nivel de riesgo"
  m <- format(seq(as.Date("2017-05-01"),as.Date("2018-01-01"),by="months"), "%Y-%B")
}

############# AGREGAR MES ADICIONAL #########################
#MESES <- c("ENERO","FEBRERO","MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO","SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE", "ENERO 2017", "FEBRERO 2017", "MARZO 2017")
MESES <- c(paste(c("MAYO", "JUNIO", "JULIO"), "2017"))

# INFO_JULIO_2017 <- INFO_JULIOCatInfi_2017
# save(INFO_JULIO_2017, file="/riesgos/DATA-DASH/RDATA_CAT_INF_2013+/INFO_JULIO_2017.Rdata")

###########  CARGAR MES ADICIONAL ##################
dir <- "/cloud/project/"
# load(paste0(dir, "INFO_MAYOCatInfi_2017.rds"))
INFO_MAYO_2017 <- readRDS(file = paste0(dir, "INFO_MAYOCatInfi_2017.rds"))

# RDatas <- list.files(dir)
# for ( i in seq(1:length(RDatas))){
#         load(paste0(dir, RDatas[i]))
# }
# rm(dir,i, RDatas)

INFO <- list(INFO_MAYO_2017)
rm(INFO_MAYO_2017)

######### ui.R ##################
ui <- fluidPage(
  theme = shinytheme("flatly"),
  #shinythemes::themeSelector(),
  
             fluidRow(column(2,
                             wellPanel(
                               #img(src = "1.png", height = 45, width = 185),
                               HTML('<center><img src="1.png" width="185"></center>'),
                               h4("DIRECCIÓN DE RIESGOS", align = "center"),
                               h5("MODELACIÓN E INDICADORES", align = "center"),
                               h6("PERFILES DE RIESGO", align = "center"),
                               h6("Comparativo Mensual", align = "center"),
                               br(),
                               selectInput("mes", "Seleccione el mes", choices = MESES, selected = "JUNIO 2017"),
                               radioButtons("grupo", "Seleccione el grupo", choices = GRUPO),
                               
                               conditionalPanel(condition = "input.conditionedPanels_matriz == 'Evolución Global' | input.conditionedPanels_matriz == 'Evolución (nuevos mes a mes)'",
                                                br(),selectInput("var_evo", SELEC_VAR, choices = var))
                               
                             )),
                      column(10,
                             navbarPage("",collapsible=T,
                                        tabPanel("Resumen",
                                                 selectInput("var", SELEC_VAR, choices = var, selected = "MONTO ADJUDICADO (en millones)"),
                                                 chartOutput("graf_resumen", "highcharts"),
                                                 div(shiny::dataTableOutput("resumen"), style = "font-size:80%")
                                        ),
                                        tabPanel("Componentes principales",
                                                 helpText("Análisis de componentes principales"),
                                                 plotOutput("graf_comp_prin"),
                                                 textOutput("ncomp"),
                                                 div(shiny::dataTableOutput("comp_prin"), style = "font-size:80%")
                                        )
                                        ,
                                        tabPanel("Ranking",
                                                 fluidRow(
                                                   column(4, selectInput("riesgo", SELEC_RIES, choices = riesgo)), br(),
                                                   column(4,downloadButton('downloadData', 'Descargar'))
                                                 ),
                                                 div(dataTableOutput("ranking"), style = "font-size:65%")
                                        )
                                        ,
                                        tabPanel("Evolución Global",
                                                 
                                                 highchartOutput("graf_evo_global"),
                                                 div(dataTableOutput("evo"), style = "font-size:80%")
                                        ),
                                        tabPanel("Evolución (nuevos mes a mes)",
                                                 #selectInput("var_NUE_evo", SELEC_VAR, choices = var),
                                                 chartOutput("NUE_evo_graf", "highcharts"),
                                                 div(dataTableOutput("NUE_evo"), style = "font-size:80%")
                                        )
                                        ,
                                        navbarMenu("Riesgo medio",
                                                   tabPanel("Evolución",
                                                            chartOutput("riesgo_medio_graf", "highcharts"),
                                                            div(dataTableOutput("riesgo_medio"), style = "font-size:80%")
                                                   ),
                                                   tabPanel("Variación riesgo medio",
                                                            chartOutput("variacion_medio_graf", "highcharts"),
                                                            div(dataTableOutput("variacion_medio"), style = "font-size:80%")
                                                   )
                                        )
                                        
                                        # ,
                                        # tabPanel("Metodología",
                                        #          htmlOutput('pdfviewer_met_riesgo'))
                                        ,
                                        id="conditionedPanels_matriz"
                             )
                      )
     
  )
)

########### server. R #######################
myoptions <- list(searching = F, searchable=F, paging = F, autoWidth=T)
myoptions1 <- list(searching = T, searchable=T, paging = T, autoWidth=T)
color1 <- "lightblue"
color2 <- "steelblue"

server <- function(input, output,session) {

  ####### REACTIVOS GLOBALES ########
    ### AÑADAIR CODIGO PARA MES ADICIONAL
    mesInput <- reactive({
      switch(input$mes,
             "MAYO 2017"=1)
    })
    
    grupoInput <- reactive({
      switch(input$grupo,
             "ENTIDADES" = 1,
             "PROVEEDORES PUBLICOS" = 3,
             "PROVEEDORES PRIVADOS" = 2)
    })
    
    zonaInput <- reactive({
      ifelse(mesInput()<50,10,
             switch(input$zona,
                    "GLOBAL"=10,
                    "Zona 1: Esmeraldas, Imbabura, Carchi, Sucumbios."=1,
                    "Zona 2: Pichincha (excepto Quito), Napo, Orellana."=2,
                    "Zona 3: Cotopaxi, Tungurahua, Chimborazo, Pastaza."=3,
                    "Zona 4: Manabi, Santo Domingo de los Tsachilas."=4,
                    "Zona 5: Santa Elena, Guayas (sin Zona 8), Bolivar, Los Rios y Galapagos."=5,
                    'Zona 6: Canar, Azuay, Morona Santiago.'=6,
                    "Zona 7: El Oro, Loja, Zamora Chinchipe."=7,
                    "Zona 8: Guayaquil, Samborondon y Duran."=8,
                    "Zona 9: Distrito Metropolitano de Quito."=9)
      )
    })
    
    
    #VARIABLES PARA RESUMEN
    
    varInput <- reactive({
      switch(input$var,
             "MONTO ADJUDICADO (en millones)" = "Monto (en Millones)",
             "EMPRESAS" = "Numero de empresas",
             "PROCEDIMIENTOS" = "PROCEDIMIENTOS")
    })
    
    # VARIABLES PARA EVOLUCION 
    varInput_evo <- reactive({
      switch(input$var_evo,
             "MONTO ADJUDICADO (en millones)" = "Monto (en Millones)",
             "EMPRESAS" = "Numero de empresas",
             "PROCEDIMIENTOS" = "PROCEDIMIENTOS")
    })
    
    # VARIABLES PARA NIVEL DE RIESGO
    riesgoInput <- reactive({
      switch(input$riesgo,
             "TODOS" = "TODOS",
             "EXTREMO" = "1. Riesgo Extremo",
             "ALTO" = "2. Riesgo Alto",
             "MEDIO" = "3. Riesgo Medio",
             "BAJO" = "4. Riesgo Bajo",
             "INSIGNIFICANTE" = "5. Riesgo Insignificante")
    })
    
    
    # FUNCIONES GLOBALES
    graf_evo <- function (datos, tipo, titulo, variable, evo=F) {
      a <- Highcharts$new()
      a$chart(type = "spline", backgroundColor = NULL)
      
      #a$chart(type = "column", backgroundColor = NULL)
      if(tipo==3){
        a$series(name= "Riesgo Extremo", data=as.numeric(datos[1, c(2:dim(datos)[2])]), dashStyle = "shortdot")
        a$series(name= "Riesgo Alto", data=as.numeric(datos[1, c(2:dim(datos)[2])]), dashStyle = "shortdot")
        a$series(name= "Riesgo Medio", data=as.numeric(datos[2, c(2:dim(datos)[2])]), dashStyle = "shortdot")
        a$series(name= "Riesgo Bajo", data=as.numeric(datos[3, c(2:dim(datos)[2])]), dashStyle = "shortdot")
        a$series(name= "Riesgo Insignficante", data=as.numeric(datos[4, c(2:dim(datos)[2])]), dashStyle = "shortdot")
        a$series(name= "TOTAL", data=as.numeric(datos[5, c(2:dim(datos)[2])]), dashStyle = "Solid")
      } else {
        a$series(name= "Riesgo Extremo", data=as.numeric(datos[1, c(2:dim(datos)[2])]), dashStyle = "shortdot")
        a$series(name= "Riesgo Alto", data=as.numeric(datos[2, c(2:dim(datos)[2])]), dashStyle = "shortdot")
        a$series(name= "Riesgo Medio", data=as.numeric(datos[3, c(2:dim(datos)[2])]), dashStyle = "shortdot")
        a$series(name= "Riesgo Bajo", data=as.numeric(datos[4, c(2:dim(datos)[2])]), dashStyle = "shortdot")
        a$series(name= "Riesgo Insignficante", data=as.numeric(datos[5, c(2:dim(datos)[2])]), dashStyle = "shortdot")
        a$series(name= "TOTAL", data=as.numeric(datos[6, c(2:dim(datos)[2])]), dashStyle = "Solid")
      }
      a$xAxis(categories = colnames(datos[1,c(2:dim(datos)[2])]))
      a$tooltip(crosshairs = TRUE, backgroundColor = "#FCFFC5",
                shared = TRUE, borderWidth = 5)
      a$exporting(enabled = TRUE) # enable exporting option
      
      a$legend(symbolWidth = 80)
      a$set(height = 400, width=1050)
      if(evo==F) {
        tx <- '- Nuevos - mes a mes'
      } 
      else if (evo=="VAR") {
        tx <- 'Variación de riesgo medio'
        variable <- ""
      } else {
        tx <- 'Nivel de riesgo medio'
        variable <- ""
      }
      
      if(titulo==1) a$title(text = paste('Entidades -', variable, tx))
      if(titulo==3) a$title(text = paste('Proveedores públicos -', variable, tx))
      if(titulo==2) a$title(text = paste('Proveedores privados -', variable, tx))
      a
    }
    
    descarga_ex <- function(datos, file, tipo){        
      wb <- createWorkbook(type="xlsx")
      
      # Define some cell styles
      # Title and sub title styles
      TITLE_STYLE <- CellStyle(wb)+ Font(wb,  heightInPoints=16, isBold=TRUE)
      
      SUB_TITLE_STYLE <- CellStyle(wb) + Font(wb,  heightInPoints=12,
                                              isItalic=TRUE, isBold=FALSE)
      
      # Styles for the data table row/column names
      TABLE_ROWNAMES_STYLE <- CellStyle(wb) + Font(wb, isBold=TRUE)
      
      TABLE_COLNAMES_STYLE <- CellStyle(wb) + Font(wb, isBold=TRUE) +
        Alignment(vertical="VERTICAL_CENTER",wrapText=TRUE, horizontal="ALIGN_CENTER") +
        Border(color="black", position=c("TOP", "BOTTOM"), 
               pen=c("BORDER_THICK", "BORDER_THICK"))+Fill(foregroundColor = "lightblue", pattern = "SOLID_FOREGROUND")
      
      sheet <- createSheet(wb, sheetName = "Ranking")
      
      # Helper function to add titles
      xlsx.addTitle<-function(sheet, rowIndex, title, titleStyle){
        rows <- createRow(sheet, rowIndex=rowIndex)
        sheetTitle <- createCell(rows, colIndex=1)
        setCellValue(sheetTitle[[1,1]], title)
        setCellStyle(sheetTitle[[1,1]], titleStyle)
      }
      
      # Add title and sub title into a worksheet
      xlsx.addTitle(sheet, rowIndex=4, 
                    title=paste("Fecha:", format(Sys.Date(), format="%Y/%m/%d")),
                    titleStyle = SUB_TITLE_STYLE)
      
      xlsx.addTitle(sheet, rowIndex=5, 
                    title="Elaborado por: Dirección de Riesgos en Contratación Pública",
                    titleStyle = SUB_TITLE_STYLE)
      
      # Add title
      if(tipo==1){
        xlsx.addTitle(sheet, rowIndex=7, 
                      paste("MATRIZ DE PERFIL DE RIESGOS - ENTIDADES CONTRATANTES -", input$mes),
                      titleStyle = TITLE_STYLE)
        
        # Add a table into a worksheet
        addDataFrame(datos[,-c(which(colnames(datos)=="CATEGORÍA"))],
                     sheet, startRow=9, startColumn=1, 
                     colnamesStyle = TABLE_COLNAMES_STYLE,
                     rownamesStyle = TABLE_ROWNAMES_STYLE,
                     row.names = FALSE)
      } else {
        
        if(tipo==3){
          xlsx.addTitle(sheet, rowIndex=7, 
                        paste("MATRIZ DE PERFIL DE RIESGOS - PROVEEDORES PÚBLICOS -", input$mes),
                        titleStyle = TITLE_STYLE)
          
        } else {
          
          if(tipo==4){
            xlsx.addTitle(sheet, rowIndex=7, 
                          paste("RIESGO POR PROCESOS - ", input$date),
                          titleStyle = TITLE_STYLE)
            
          } else {
            
            if(tipo==5){
              xlsx.addTitle(sheet, rowIndex=7, 
                            paste("RIESGO POR PROCESOS CON OFERTAS - ", input$date),
                            titleStyle = TITLE_STYLE)
            } else {
              if(tipo==6){
                xlsx.addTitle(sheet, rowIndex=7, 
                              paste("RIESGO POR PROCESO - CONSOLIDADO"),
                              titleStyle = TITLE_STYLE)
              } else {
                if(tipo==7){
                  xlsx.addTitle(sheet, rowIndex=7, 
                                paste("RIESGO POR PROCESO MUNICIPIO DE QUITO", input$fecha_MQ),
                                titleStyle = TITLE_STYLE)
                } else {
                  xlsx.addTitle(sheet, rowIndex=7,
                                paste("MATRIZ DE PERFIL DE RIESGOS - PROVEEDORES PRIVADOS -", input$mes),
                                titleStyle = TITLE_STYLE)
                }
              }
            }
          }
        }
        # Add a table into a worksheet
        addDataFrame(datos,
                     sheet, startRow=9, startColumn=1,
                     colnamesStyle = TABLE_COLNAMES_STYLE,
                     rownamesStyle = TABLE_ROWNAMES_STYLE,
                     row.names = FALSE)
      }
      
      # Change column width
      setColumnWidth(sheet, colIndex=c(1:ncol(datos)), colWidth=20)
      
      # image
      # addPicture("/riesgos/apps/matriz/www/1.png", sheet, scale=0.28, startRow = 1, startColumn = 1)
      addPicture("/cloud/project/www/1.png", sheet, scale=0.28, startRow = 1, startColumn = 1)
      
      
      # Save the workbook to a file...
      saveWorkbook(wb, file)
    }
    
    resumen <- function(datos){
      colnames(datos) <-  c("Nivel de Riesgo", m[1:(dim(datos)[2]-1)])
      a <- t(data.frame("TOTAL"=colSums(datos[,-1], na.rm = T)))
      datos <- rbind(datos, datos[1,])
      datos[dim(datos)[1],1] <- "TOTAL"
      datos[dim(datos)[1],c(2:dim(datos)[2])] <- a
      colnames(datos) <-  c("Nivel de Riesgo", m[1:(dim(datos)[2]-1)])
      datos}
    
    resumen_nuevos <- function(datos){
      datos <- datos[-dim(datos)[1],]
      b <- list(0)
      for( i in c(dim(datos)[2]:2)){
        ifelse(i==2,
               b[[i-1]] <- datos[,i]-datos[,i],
               b[[i-1]] <- datos[,i]-datos[, c(i-1)])
      }
      bb <- do.call(cbind, b)
      datos[,-1] <- bb
      a <- t(data.frame("TOTAL"=colSums(datos[,-1])))
      datos <- rbind(datos, datos[1,])
      datos[dim(datos)[1],1] <- "TOTAL"
      datos[dim(datos)[1],c(2:dim(datos)[2])] <- a
      colnames(datos) <-  c("Nivel de Riesgo", m[1:(dim(datos)[2]-1)])
      datos}

    pie <- function (datos, v, y, titulo) {
      n1 <- Highcharts$new()
      n1 <- hPlot(v, y, data = datos, type = "pie", options3d = list(enabled = TRUE, alpha = 70, beta = 0))
      
      n1$addParams(height = 400, width = 1050)
      n1$exporting(sourceWidth = 1000, sourceHeight = 400)
      if(titulo==1) n1$title(text = paste("Resumen Entidades - ", y," - ", input$mes))
      if(titulo==3) n1$title(text = paste("Resumen Proveedores Públicos - ", y," - ", input$mes))
      if(titulo==2) n1$title(text = paste("Resumen Proveedores Privados - ", y," - ", input$mes))
      if(titulo==4) n1$title(text = "")
      n1
    }
    
    variacion_relativa <- function(datos){
      variacion <- list(0)
      for( i in c(dim(datos)[2]:2)){
        ifelse(i==2,
               variacion[[i-1]] <- datos[,i]-datos[,i],
               variacion[[i-1]] <- (datos[,i]-datos[, c(i-1)])/datos[, c(i-1)])
        variacion[[i-1]][is.infinite(variacion[[i-1]])]<-0
        variacion[[i-1]][is.nan(variacion[[i-1]])]<-0
      }
      variacion[[i-1]][(variacion[[i-1]])] <- 0
      bb <- do.call(cbind, variacion)
      datos[,-1] <- bb
      datos[,-2]
    }
    
    graf_evo_global <- function(datos, variable, titulo) {
      a <- unlist(c(datos[1]))
      datos <- as.data.frame(t(datos[,c(2:dim(datos)[2])]))
      colnames(datos) <- a
      Mes <- rownames(datos)
      datos <- cbind(Mes, datos)
      
        if(titulo==1) y <-  "- Entidades -"
        if(titulo==2) y <-  "- Proveedores privados -"
        if(titulo==3) y <-  "- Proveedores públicos -"
        
        hc <- highchart() %>%
          hc_add_theme(hc_theme_smpl()) %>%
          hc_title(text = paste("Evolución Global", y, variable)) %>%
          hc_xAxis(categories = datos[,1]) %>%
          hc_yAxis(title = list(text = variable),align = "left")  %>%
          hc_add_series(name = "Extremo", data = datos$'1. Riesgo Extremo') %>%
          hc_add_series(name = "Alto", data = datos$'2. Riesgo Alto') %>%
          hc_add_series(name = "Medio", data = datos$'3. Riesgo Medio') %>%
          hc_add_series(name = "Bajo", data = datos$'4. Riesgo Bajo') %>%
          hc_add_series(name = "Insignificante", data = datos$'5. Riesgo Insignificante') %>%
          hc_chart(type = "column",
                   options3d = list(enabled = F, beta = 15, alpha = 15)) %>%
          hc_tooltip(crosshairs = TRUE, backgroundColor = "#FCFFC5",
                     shared = TRUE, borderWidth = 5) %>%
          hc_exporting(enabled = TRUE) # enable exporting option
     hc}
    
  
  formato_resumen <- function(data){
    nom_colum <- colnames(data)
    nom_fil <- data[,1]
    data <- sapply(data[,c(2:7)],as.numeric)
    rownames(data) <- nom_fil
    data <- cbind(sapply(data[,c(1)],comma_format()),
                  sapply(data[,c(2)], percent_format()),
                  sapply(data[,c(3)],comma_format()),
                  sapply(data[,c(4)], percent_format()),
                  sapply(data[,c(5)],dollar_format()),
                  sapply(data[,c(6)], percent_format()))
    colnames(data) <- nom_colum[2:7]
    as.data.frame(data)
  }
  
  
  #################### OUTPUTS ################
  {
    
    point <- format_format(big.mark = ".", decimal.mark = ",", scientific = FALSE)
    
    # RESUMEN
    output$resumen <- shiny::renderDataTable({
      a <- mesInput()
      x <- grupoInput()
      xx <- zonaInput()
      #formato_resumen(INFO[[a]][[x]][[xx]]$resumen)}, options = myoptions)
      INFO[[a]][[x]][[xx]]$resumen$`PROCEDIMIENTOS`<- point(as.numeric(INFO[[a]][[x]][[xx]]$resumen$`PROCEDIMIENTOS`))
      
      INFO[[a]][[x]][[xx]]$resumen$`Monto (en Millones)`<- sapply(as.numeric(INFO[[a]][[x]][[xx]]$resumen$`Monto (en Millones)`),dollar_format())
      INFO[[a]][[x]][[xx]]$resumen[[3]]<- sapply(as.numeric(INFO[[a]][[x]][[xx]]$resumen[[3]])/100,percent_format())
      INFO[[a]][[x]][[xx]]$resumen[[5]]<- sapply(as.numeric(INFO[[a]][[x]][[xx]]$resumen[[5]])/100,percent_format())
      INFO[[a]][[x]][[xx]]$resumen[[7]]<- sapply(as.numeric(INFO[[a]][[x]][[xx]]$resumen[[7]])/100,percent_format())
      
      INFO[[a]][[x]][[xx]]$resumen}, options = myoptions)
    
    output$graf_resumen <- renderChart2({
      a <- mesInput()
      x <- grupoInput()
      xx <- zonaInput()
      datos <- INFO[[a]][[x]][[xx]]$tabla1
      pie(datos=datos,v="Nivel de Riesgo",y=varInput(), titulo=x)
    })
    
    # COMPONENTES PRINCIPALES
    output$comp_prin <- shiny::renderDataTable({
      a <- mesInput()
      x <- grupoInput()
      xx <- zonaInput()
      INFO[[a]][[x]][[xx]]$importance}, options = myoptions)
    
    output$graf_comp_prin <- renderPlot({
      a <- mesInput()
      x <- grupoInput()
      xx <- zonaInput()
      par(mfrow=c(1,2))
      screeplot(INFO[[a]][[x]][[xx]]$acp1,main = paste("Varianza capturada por las componentes", input$grupo, input$mes), col=color1)
      abline(1,0)
      screeplot(INFO[[a]][[x]][[xx]]$acp1,type="lines",main = paste("Gráfico de Sedimentación", input$grupo, input$mes), col=color2)
    })
    
    output$ncomp <- renderText({
      a <- mesInput()
      x <- grupoInput()
      xx <- zonaInput()
      paste("- Se debe retener ", sum((INFO[[a]][[x]][[xx]]$acp1$sdev)^2 >1), "componentes, los cuales representan un (",
            round(as.numeric(summary(INFO[[a]][[x]][[xx]]$acp1)$importance[3, sum((INFO[[a]][[x]][[xx]]$acp1$sdev)^2 >1)])*100,2),
            "%) de la informacion.")
    })
    
    # RANKING
    output$ranking <- renderDataTable({
      a <- mesInput()
      x <- grupoInput()
      xx <- zonaInput()
      #INFO[[a]][[x]][[xx]]$ranking$`MONTO ADJUDICADO` <- sapply(INFO[[a]][[x]][[xx]]$ranking$`MONTO ADJUDICADO`,dollar_format())
      datos <- INFO[[a]][[x]][[xx]]$ranking
      n_col <- dim(datos)[2]
      var_cam <- riesgoInput()
      #if(x==3 & var_cam=="1. Riesgo Extremo") datos_f <- matrix("NO HAY EXTREMOS",1,1)
      
        ifelse(var_cam=="TODOS",
               # datos_f <- datos[, c(1:4,9,10,n_col-2, n_col-1, n_col)],
               # datos_f <- datos[, c(1:4,9,10,n_col-2, n_col-1, n_col)] %>% filter (datos$'NIVEL DE RIESGO'==var_cam)
               datos_f <- datos,
               datos_f <- datos %>% filter (datos$'NIVEL DE RIESGO'==var_cam)
        )
      
      datatable(datos_f, filter = 'top', options = list(
        pageLength = 10, autoWidth = TRUE)) %>% formatCurrency(c('MONTO ADJUDICADO'))
    }, options = myoptions
    )
    
    archivo <- reactive(
            ifelse(grupoInput()==2,'.txt','.xlsx')
    )
    
    output$downloadData <- downloadHandler(
      #filename = function() { paste("Perfiles de Riesgo", '.txt') },
      filename = function() { paste("Perfiles de Riesgo", archivo()) },
      content = function(file) {
        a <- mesInput()
        x <- grupoInput()
        xx <- zonaInput()
        datos <- INFO[[a]][[x]][[xx]]$ranking
        var_cam <- riesgoInput()
        
        ifelse(var_cam=="TODOS",
               datos_f <- datos,
               datos_f <- datos %>% filter (datos$'NIVEL DE RIESGO'==var_cam)
        )
        if (x==2) {
          write.csv2(datos_f, file)
        }
        else {
          descarga_ex(datos_f, file, tipo=x)}
      }
    )
    
    
    datos_agru <- function(data,val){
      ifelse(dim(data)[1]==4,
             {
               data1 <- tbl_df(0)
               data1[1] <- "1. Riesgo Extremo"
               data1[2] <- val
               colnames(data1) <- names(data)
               data1[2:5,] <- data
             }, {data1 = data})
      data1
    }
    
    
    evolucion_global <- reactive({ 
            x <- grupoInput() #x<- 1
            xx <- zonaInput() #xx<-10
            variable <- varInput_evo() #variable <- "PROCEDIMIENTOS"
            datos1 <- list(0)
            datos <- list(0)
            mn <- which(MESES==input$mes) #mn<-4
            
            if(mn==1) {
                    for (i in c(1:2)) {
                            datos1[[i]] <- INFO[[i]][[x]][[xx]]$tabla1[, c("Nivel de Riesgo", variable)]
                            datos[[i]] <- merge(datos1[[1]], datos1[[i]], by="Nivel de Riesgo", all.x=T)
                            
                    }
                    f <- datos[[2]]
            }
            if(mn>1) {
                    for (i in c(1:mn)) {
                            datos1[[i]] <- INFO[[i]][[x]][[xx]]$tabla1[, c("Nivel de Riesgo", variable)]
                            datos[[1]] <- datos1[[1]]
                            if(i>1) {
                                    datos[[i]] <- merge(datos[[i-1]], datos1[[i]], by="Nivel de Riesgo", all.x=T)
                                    }
                    }
                    f <- datos[[mn]]
            }
            resumen(f)
            
    })
    
    
    riesgo_medio <- reactive({
            x <- grupoInput() #x<- 1
            xx <- zonaInput() #xx<-10
            variable <- varInput_evo() #variable <- "PROCEDIMIENTOS"
            mn <- which(MESES==input$mes)
            datos <- list(0)
            datos1 <- list(0)
            if(mn==1) {
                    for (i in c(1:2)) {
                            datos1[[i]] <- group_by(INFO[[i]][[x]][[xx]]$ranking[, c("PUNTAJE (0-1)","NIVEL DE RIESGO")], `NIVEL DE RIESGO`) %>% 
                                    summarize(Promedio=round(mean(`PUNTAJE (0-1)`),3))
                            datos[[i]] <- merge(datos1[[1]], datos1[[i]], by="NIVEL DE RIESGO", all.x=T)
                    }
                    f <- datos[[2]]
                    }
            if(mn>1) {
                    for (i in c(1:mn)) {
                            datos1[[i]] <- group_by(INFO[[i]][[x]][[xx]]$ranking[, c("PUNTAJE (0-1)","NIVEL DE RIESGO")], `NIVEL DE RIESGO`) %>% 
                                    summarize(Promedio=round(mean(`PUNTAJE (0-1)`),3))
                            datos[[1]] <- datos1[[1]]
                            if(i>1) {
                                    datos[[i]] <- merge(datos[[i-1]], datos1[[i]], by="NIVEL DE RIESGO", all.x=T)
                            }
                    }
                    f <- datos[[mn]]
            }
            resumen(f)
            })
    
    
    
    ##### EVOLUCION ######
    
    output$graf_evo_global <- renderHighchart({
            x <- grupoInput()
      graf_evo_global(evolucion_global(), varInput_evo(), titulo=x)
    })
    
    output$evo <- renderDataTable({
      evolucion_global()
    })
    
    output$NUE_evo <- renderDataTable({
      resumen_nuevos(evolucion_global())}
      )
    
    output$NUE_evo_graf <- renderChart2({
            x <- grupoInput()
      graf_evo(resumen_nuevos(evolucion_global()), tipo=x, titulo=x,varInput_evo())
    })
    
    ######## INDICADOR EVOLUCION ###############
    output$riesgo_medio <- renderDataTable({
            riesgo_medio()
    })
    
    output$riesgo_medio_graf <- renderChart2({
            x <- grupoInput()
      graf_evo(riesgo_medio(), tipo=x, titulo=x,varInput_evo(), evo=T)
    })
    

    ####### VARIACION INDICADOR EVOLUCION ####################
    output$variacion_medio <- renderDataTable({
      x <- grupoInput()
      variacion_relativa(riesgo_medio())
    })
    
    output$variacion_medio_graf <- renderChart2({
      x <- grupoInput()
      graf_evo(variacion_relativa(riesgo_medio()), tipo=x, titulo=x,variable, evo="VAR")
    })
    
    
    
    }
  }

shinyApp(ui = ui, server = server)
